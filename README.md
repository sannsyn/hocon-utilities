# README #

This is a small library of hocon utilities. Hocon is an acronym for «Human-Optimized Config Object Notation», a format conceived by Typesafe. It is a superset of JSON and the java properties format, accepting comments and dealing very liberally with its structure. More information is found on [their git project page](https://github.com/typesafehub/config).

Nice examples and explanations of the HOCON syntax is found on [the Akka framework website](http://getakka.net/docs/concepts/hocon)

# Play framework #
One of the components in this repo is a BodyParser in java for [the Play Framework](https://www.playframework.com/). Since Typesafe is also behind Play, it is funny that this BodyParser is not supplied by default. You need Play version 2.5 or higher to use the HOCON BodyParser with java.

## Licence ##

* The code in this repo is freely available to all.
* You may do with it as you please.
* Use with no restrictions, and at your own risk.