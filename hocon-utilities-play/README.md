# README #

This subproject contains BodyParsers in java for [the Play! Framework](https://www.playframework.com/). Since Typesafe is also behind Play, it is funny that this BodyParser is not supplied by default. You need Play version 2.5 or higher to use the HOCON BodyParser with java.

To use one of these bodyparsers in a Play! project, do as follows:

* include files from this project or add `libraryDependencies += "com.sannsyn.hocon-utilities" % "all" % "2.0"` to your `build.sbt` file
* import a body parser (e.g. `com.sannsyn.hoconutilities.playframework.bodyparser.TolerantHocon`) to your controller
* add an annotation (e.g. `@BodyParser.Of(TolerantHocon.class)`) to the method that responds to an action call
* Pick up your config object thus: `Config payload = request().body().as(Config.class);`

Simple, huh?

## Licence ##

* The code in this repo is freely available to all.
* You may do with it as you please.
* Use with no restrictions, and at your own risk.