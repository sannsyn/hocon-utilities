package com.sannsyn.hoconutilities.playframework.bodyparser;

import akka.util.ByteString;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.api.http.HttpConfiguration;
import play.http.HttpErrorHandler;
import play.mvc.BodyParser;
import play.mvc.Http;

import javax.inject.Inject;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Parses the body of an http request into a Config object (if no exception is thrown)
 * without checking the Content-Type.
 */
public class TolerantHocon extends BodyParser.BufferingBodyParser<Config> {

  protected TolerantHocon(long maxLength, HttpErrorHandler httpErrorHandler) {
    super(maxLength, httpErrorHandler, "Error parsing hocon body.");
  }

  @Inject
  protected TolerantHocon(HttpConfiguration httpConfiguration, HttpErrorHandler httpErrorHandler) {
    super(httpConfiguration, httpErrorHandler, "Error parsing hocon body.");
  }

  @Override
  protected Config parse(Http.RequestHeader requestHeader, ByteString byteString) throws Exception {
    return ConfigFactory.parseReader(new InputStreamReader(byteString.iterator().asInputStream(), StandardCharsets.UTF_8));
  }

}
