package com.sannsyn.hoconutilities.playframework.bodyparser;

import akka.util.ByteString;
import com.typesafe.config.Config;
import play.api.http.HttpConfiguration;
import play.http.HttpErrorHandler;
import play.libs.F;
import play.libs.streams.Accumulator;
import play.mvc.BodyParsers;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;

/**
 * Parses the body of an http request into a Config object (if no exception is thrown)
 * if the Content-Type is one of the following:
 * <ul>
 *   <li>text/plain</li>
 *   <li>text/json</li>
 *   <li>application/json</li>
 *   <li>application/hocon (as specified <a href="https://github.com/typesafehub/config/blob/master/HOCON.md#mime-type">here</a>)</li>
 * </ul>
 */
public class Hocon extends TolerantHocon {
  private final HttpErrorHandler errorHandler;

  public Hocon(long maxLength, HttpErrorHandler errorHandler) {
    super(maxLength, errorHandler);
    this.errorHandler = errorHandler;
  }

  @Inject
  public Hocon(HttpConfiguration httpConfiguration, HttpErrorHandler errorHandler) {
    super(httpConfiguration, errorHandler);
    this.errorHandler = errorHandler;
  }

  @Override
  public Accumulator<ByteString, F.Either<Result, Config>> apply(Http.RequestHeader request) {
    return BodyParsers.validateContentType(errorHandler, request, "Expected hocon text",
            ct -> ct.equalsIgnoreCase("application/hocon")
                    || ct.equalsIgnoreCase("application/json")
                    || ct.equalsIgnoreCase("text/json")
                    || ct.equalsIgnoreCase("text/plain"),
            super::apply
    );
  }}
