package com.sannsyn.hoconutilities.playframework.bodyparser;

import akka.util.ByteString;
import com.typesafe.config.Config;
import play.api.http.HttpConfiguration;
import play.http.HttpErrorHandler;
import play.libs.F;
import play.libs.streams.Accumulator;
import play.mvc.BodyParsers;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;

/**
 * Parses the body of an http request into a Config object (if no exception is thrown)
 * if and only if the Content-Type is <b>application/hocon</b>.
 */
public class StrictHocon extends TolerantHocon {
  private final HttpErrorHandler errorHandler;

  public StrictHocon(long maxLength, HttpErrorHandler errorHandler) {
    super(maxLength, errorHandler);
    this.errorHandler = errorHandler;
  }

  @Inject
  public StrictHocon(HttpConfiguration httpConfiguration, HttpErrorHandler errorHandler) {
    super(httpConfiguration, errorHandler);
    this.errorHandler = errorHandler;
  }

  @Override
  public Accumulator<ByteString, F.Either<Result, Config>> apply(Http.RequestHeader request) {
    return BodyParsers.validateContentType(errorHandler, request, "Expected content type to be application/hocon",
            ct -> ct.equalsIgnoreCase("application/hocon"),
            super::apply
    );
  }}
