To use these classes in a Play! Framework project, this is all you have to do

* Use Play! version 2.5 or above
* In your controller, apply one of these annotations to any method:
     	  	     @BodyParser.Of(StrictHocon.class)
     	  	     @BodyParser.Of(Hocon.class)
     	  	     @BodyParser.Of(TolerantHocon.class)

That's all! you now get the hocon object in your code like this:

Config config = request().body().as(Config.class);
Logger.debug("Here's the incoming config object: " + config.root().render(ConfigRenderOptions.concise().setComments(true)));


The TolerantHocon and StrictHocon classes are like the «Tolerant» and not-defined-as-tolerant classes for other
content types shipped with the Play! Framework, i.e. StrictHocon will only accept incoming hocon documents if
their content-type is set to «application/hocon», and TolerantHocon won't bother checking the content-type
header field at all, but simply fail if the submitted data cannot be converted into hocon. As a third option,
there is the class simply named Hocon, which accepts documents defined as
      * application/hocon
      * application/json
      * text/json, and
      * text/plain
This is logical. Since json is a subset of hocon, all valid json documents are valid hocon documents too.

December 2015,
Sannsyn
