package com.sannsyn.hoconutilities.playframework.bodyparser;

import play.http.HttpErrorHandler;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This example error handler will return a JSON result whenever something goes wrong
 * in receiving payloads as HOCON. To use it, override the <tt>super()</tt> call in the
 * BodyParserConstructor, i.e. like this:<br/>
 * <tt>super(maxLength, new HoconErrorHandler(), "Error parsing hocon body.");</tt>
 */
public class HoconErrorHandler implements HttpErrorHandler {

    /**
     * Invoked when a client error occurs, that is, an error in the 4xx series.
     *
     * @param request    The request that caused the client error.
     * @param statusCode The error status code.  Must be greater or equal to 400, and less than 500.
     * @param message    The error message.
     */
    @Override
    public CompletionStage<Result> onClientError(Http.RequestHeader request, int statusCode, String message) {
        return CompletableFuture.supplyAsync(() ->
                Results.status(400, "{\"Error\": \"Malformed HOCON payload\",\"Reason\":\"" + message + "\"}")
                        .as("application/json"));
    }

    /**
     * Invoked when a server error occurs.
     *
     * @param request   The request that triggered the server error.
     * @param exception The server error.
     */
    @Override
    public CompletionStage<Result> onServerError(Http.RequestHeader request, Throwable exception) {
        return CompletableFuture.supplyAsync(() ->
                Results.status(500, "{\"Error\": \"Errare serveris est\",\"Reason\":\"" + exception.getLocalizedMessage() + "\"}")
                        .as("application/json"));
    }
}
