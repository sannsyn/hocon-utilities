import com.sannsyn.hoconutilities.ConfigComparator;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigRenderOptions;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by hallvard on 04.08.16.
 */
public class ConfigComparatorTest {

    @Test
    public void CheckAreIdentical() {
        Config a = ConfigFactory.parseString("xd=null,a=[2,3],da.x:5, da.b.c.d.e:end, da.b.c.f:99, ku:q");
        Config b = ConfigFactory.parseString("a=[2,3],ku=q,da{x=5,b{c{d{e:end},f=99}}}");
        ConfigComparator comparator = new ConfigComparator(a,b);
        System.out.println("Not alike: : " + a.root().render(ConfigRenderOptions.concise()) + ", " + b.root().render(ConfigRenderOptions.concise()));
        boolean samesame = comparator.areIdentical();
        assertFalse(samesame);

        a = ConfigFactory.parseString("x=1");
        b = ConfigFactory.parseString("x=2");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.areIdentical();
        System.out.println("1 og 2 should not be treated as identical values: " + samesame);
        assertFalse(samesame);

        a = ConfigFactory.parseString("x=1");
        b = ConfigFactory.parseString("x=1, n=null");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.areIdentical();
        System.out.println("Explicit null and a missing value should not be treated as identical: " + samesame);
        assertFalse(samesame);

    }

    @Test
    public void CheckNullHandling() {
        Config a = ConfigFactory.parseString("one: 1");
        Config b = null;
        try {
            ConfigComparator comparator = new ConfigComparator(a, b);
        } catch (IllegalArgumentException iae) {
            System.out.println("Expected exception: " + iae.getLocalizedMessage());
            assert true;
            return;
        }

        fail("Null values shouldn't be usable.");
    }

    @Test
    public void CheckHasSamePaths() {
        Config a = ConfigFactory.parseString("one: 1");
        Config b = ConfigFactory.parseString("one: 1");
        ConfigComparator comparator = new ConfigComparator(a,b);
        boolean samesame = comparator.haveSamePaths();
        System.out.println("To identical ones: " + a + ", " + b + " (same: " + samesame + ")");
        assertTrue(samesame);

        b = ConfigFactory.parseString("one: 2");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.haveSamePaths();
        System.out.println("Same key, different value: " + a + ", " + b + " (same: " + samesame + ")");
        assertTrue(samesame);

        b = ConfigFactory.parseString("");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.haveSamePaths();
        System.out.println("Compare to an empty config: " + a + ", " + b + " (same: " + samesame + ")");
        assertFalse(samesame);

        a = ConfigFactory.parseString("");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.haveSamePaths();
        System.out.println("Compare two empty configs: " + a + ", " + b + " (same: " + samesame + ")");
        assertTrue(samesame);

        a = ConfigFactory.parseString("one: 1");
        b = ConfigFactory.parseString("one: 1, two: 2");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.haveSamePaths();
        System.out.println("More in b than in a: " + a + ", " + b + " (same: " + samesame + ")");
        assertFalse(samesame);

        a = ConfigFactory.parseString("one: 1, two: null");
        b = ConfigFactory.parseString("one: 1");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.haveSamePaths();
        System.out.println("Two almost the same (do not have same paths): " + a + ", " + b + " (same: " + samesame + ")");
        assertFalse(samesame);

        a = ConfigFactory.parseString("a.x:5, a.b.c.d.e:end, a.b.c.f:99");
        b = ConfigFactory.parseString("a{x=5,b{c{d{e:end},f=g}}}");
        comparator = new ConfigComparator(a,b);
        samesame = comparator.haveSamePaths();
        System.out.println("Not constructed in the same manner, but with the same result: " + a.root().render(ConfigRenderOptions.concise()) + ", " + b.root().render(ConfigRenderOptions.concise()) + " (same: " + samesame + ")");
        assertTrue(samesame);

    }

}