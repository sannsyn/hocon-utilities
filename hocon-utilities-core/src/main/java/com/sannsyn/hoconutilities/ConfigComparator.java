package com.sannsyn.hoconutilities;

import com.typesafe.config.*;

import java.util.*;

/**
 * Created by hallvard on 22.11.16.
 */
public class ConfigComparator {

    private final Config diffConfig;
    private final Config lefthandsideConfig;
    private final Config righthandsideConfig;
    private final boolean haveSamePaths;
    private final boolean areIdentical;

    public ConfigComparator(Config lefthandsideConfig, Config righthandsideConfig) {
        if (lefthandsideConfig == null || righthandsideConfig == null) {
            throw new IllegalArgumentException("Cannot compare null objects.");
        }
        this.lefthandsideConfig = lefthandsideConfig;
        this.righthandsideConfig = righthandsideConfig;
        this.diffConfig = this.diff(lefthandsideConfig, righthandsideConfig, "", ConfigFactory.empty());
        haveSamePaths = commonChecks() && missingLeft().isEmpty() && missingRight().isEmpty();
        areIdentical = commonChecks() && diffConfig.isEmpty();
    }

    /**
     * Returns all path entries that exist in both <tt>Config</tt>s, but
     * do not have the same type.
     */
    public List<String> valueTypeDifferences() {
        return fromDiff("valuetypeMismatches");
    }

    /**
     * Returns all path entries that exist in both <tt>Config</tt>s, but
     * do not have the same value. This will include all path entries
     * returned from [valueTypeDifferences].
     */
    public List<String> unequalValues() {
        return fromDiff("valuesUnequal");
    }

    /**
     * Returns all path entries that exist in [righthandsideConfig], but not in [lefthandsideConfig]
     */
    public List<String> missingLeft() {
        return fromDiff("lacks.left");
    }

    /**
     * Returns all path entries that exist in [lefthandsideConfig], but not in [righthandsideConfig]
     */
    public List<String> missingRight() {
        return fromDiff("lacks.right");
    }

    /**
     * Returns <tt>true</tt> if the two Config objects have the same paths. Values in the paths are ignored, so this is
     * most useful when checking if any paths are left out from a config. Paths with null values are left out of the
     * comparison.
     */
    public boolean haveSamePaths() {
        return haveSamePaths;
    }

    /**
     * Returns <tt>true</tt> if the two Config objects have the same paths and the same values in the paths. Null values
     * are *not*> ignored. If the lefthandside config has a path with a null value, config b should have the same
     * path with a null value in order for them to be identical. The order of keys within the different objects in the
     * config obects makes no difference, nor do the type of number values (int, long, float etc.)
     */
    public boolean areIdentical() {
        return areIdentical;
    }

    /////////////////////////////////////////
    // Internal methods
    /////////////////////////////////////////

    /**
     * Two initial checks that are the same for all the three checks in this class.
     * This method is for internal build and is therefore private.
     */
    private boolean commonChecks() {

        if (lefthandsideConfig.isResolved() != righthandsideConfig.isResolved()) {
            return false;
        }

        if (lefthandsideConfig.entrySet().size() != righthandsideConfig.entrySet().size()) {
            return false;
        }

        return true;

    }

    /**
     * Parses the two configs and makes a diff stored in <tt>diffConfig</tt>.
     * This method works recursively, and therefore receives the Configs as params.
     * This method is for internal build and is therefore private.
     */
    private Config diff(Config left, Config right, String path, Config config) {
        Config lConfig = config;

        ConfigObject aObj = left.root();
        ConfigObject bObj = right.root();

        for (String key : aObj.keySet()) {
            String here = path.isEmpty() ? key : path + "." + key;
            if (!bObj.containsKey(key)) {
                lConfig = addToDiff("lacks.right", here, lConfig);
                continue;
            }
            ConfigValue aVal = aObj.get(key);
            ConfigValue bVal = bObj.get(key); // could we get a java null here? an NPE?
            if (aVal.valueType() != bVal.valueType()) {
                lConfig = addToDiff("valuetypeMismatches", here, lConfig);
                lConfig = addToDiff("valuesUnequal", here, lConfig);
                continue;
            }

            if (aVal.valueType() == ConfigValueType.OBJECT) {
                Config subConfig = diff(((ConfigObject)aVal).toConfig(), ((ConfigObject)bVal).toConfig(), here, lConfig);
                lConfig = subConfig.withFallback(lConfig);
            } else {
                if (aVal.valueType() != ConfigValueType.NULL) {
                    if (!aVal.unwrapped().equals(bVal.unwrapped())) {
                        lConfig = addToDiff("valuesUnequal", here, lConfig);
                    }
                } else {
                    // unwrapping ConfigValueType.NULL yields a null object, on which we cannot run .equals(), so:
                    if (bVal.valueType() != ConfigValueType.NULL) {
                        lConfig = addToDiff("valuesUnequal", here, lConfig);
                    }
                }
            }
        }
        // OK, so now we have diffed from the lefthandside pov.
        // Now look for values to the right that lacks to the left:
        for (String key : bObj.keySet()) {
            String here = path.isEmpty() ? key : path + "." + key;
            if (!aObj.containsKey(key)) {
                lConfig = addToDiff("lacks.left", here, lConfig);
            }
        }
        return lConfig;
    }

    /**
     * This method is for internal build and is therefore private.
     */
    private Config addToDiff(String path, String toAdd, Config config) {
        List<String> toAddTo;
        try {
            toAddTo = config.getStringList(path);
        } catch (Exception e) {
            toAddTo = new ArrayList<String>();
        }

        toAddTo.add(toAdd);
        return config.withValue(path, ConfigValueFactory.fromIterable(toAddTo));
    }


    /**
     * Returns the string list stored at <tt>path</tt>.
     * This method is for internal build and is therefore private.
     */
    private List<String> fromDiff(String path) {
        try {
            return diffConfig.getStringList(path);
        } catch (Throwable t) {
            return Collections.<String>emptyList();
        }
    }
}
