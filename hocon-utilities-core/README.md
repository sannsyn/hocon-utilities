# THE HOCON COMPARATOR #

This core package in the HOCON utilities project contains simply a comparator. The nice thing about this is that it does not compare the hocons as text files but as configs. You may compare a JSON file to a HOCON file. If keys and values are equal, the files are deemed identical, regardless of comments in the hocon format, «missing» quotes etc.

## Licence ##

* The code in this repo is freely available to all.
* You may do with it as you please.
* Use with no restrictions, and at your own risk.