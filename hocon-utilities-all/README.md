# THE «ALL» SHORTCUT #

This sub project is simply a shorthand import for all the other HOCON-utilities subprojects.

## Licence ##

* The code in this repo is freely available to all.
* You may do with it as you please.
* Use with no restrictions, and at your own risk.